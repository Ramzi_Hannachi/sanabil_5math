package utils;
import android.app.Activity;
import android.content.Context;
import android.media.MediaPlayer;

public class Utils {


	public static void animateFad(Activity pActivity)
	{
		pActivity.overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
	}

	public static void stopMediaPlayer(MediaPlayer mp){
		if (mp != null) {
			mp.stop();
			mp.release();
		}
	}
	public void startMediaPlayer(MediaPlayer mp){
		if (mp != null) {
			mp.stop();
			mp.release();
		}
	}
}
