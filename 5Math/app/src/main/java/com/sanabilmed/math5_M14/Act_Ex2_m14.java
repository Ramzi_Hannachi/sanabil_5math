package com.sanabilmed.math5_M14;

import java.util.Random;
import java.util.Vector;

import utils.Scalable;
import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import com.sanabilmed.math5_M.Act_Modules;
import com.sanabilmed.science4N.R;



public class Act_Ex2_m14 extends Activity implements OnClickListener
{
	private ImageView rep,ex1, ex3, audio,
	imgVer, imgCorr, exit, home, point_interrogation;
	private MediaPlayer mp;
	protected Vector<View> images;
	AnimationSet animation;

	private Button button1_1 , button1_2 ,
	button2_1 , button2_2 , button2_3 , button2_4 ,
	button3_1 , button3_2 , button3_3 , button3_4 ,
	button4_1 , button4_2 , button4_3 , button4_4 ,
	button5_1 , button5_2 , button5_3 , button5_4 ,
	button6_1 , button6_2 , button6_3 ;

	private boolean bloc_1=false , bloc_2=false , bloc_3=false , bloc_4=false , bloc_5=false , bloc_6=false ;

	private String test_1="" , test_2="" , test_3="" , test_4="" , test_5="" , test_6="" ; 
	Activity act;
	ClipData data;
	int randomInt;

	private static final long active = 3000;
	private static final int STOP = 0;
	private Message msg;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onCreate(Bundle savedInstanceState)
	{
		setContentView(R.layout.act_ex2_m14);

		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);
		act = this;
		ex3 = (ImageView) findViewById(R.id.ex_3);
		ex1 = (ImageView) findViewById(R.id.ex_1);

		imgCorr = (ImageView) findViewById(R.id.imgCorr);
		imgVer = (ImageView) findViewById(R.id.imgVer);
		rep = (ImageView) findViewById(R.id.repeat);
		audio = (ImageView) findViewById(R.id.jouerSon);

		point_interrogation=(ImageView) findViewById(R.id.point_interrogation);

		button1_1 = (Button) findViewById(R.id.button1_1);
		button1_2 = (Button) findViewById(R.id.button1_2);
		button2_1 = (Button) findViewById(R.id.button2_1);
		button2_2 = (Button) findViewById(R.id.button2_2);
		button2_3 = (Button) findViewById(R.id.button2_3);
		button2_4 = (Button) findViewById(R.id.button2_4);
		button3_1 = (Button) findViewById(R.id.button3_1);
		button3_2 = (Button) findViewById(R.id.button3_2);
		button3_3 = (Button) findViewById(R.id.button3_3);
		button3_4 = (Button) findViewById(R.id.button3_4);
		button4_1 = (Button) findViewById(R.id.button4_1);
		button4_2 = (Button) findViewById(R.id.button4_2);
		button4_3 = (Button) findViewById(R.id.button4_3);
		button4_4 = (Button) findViewById(R.id.button4_4);
		button5_1 = (Button) findViewById(R.id.button5_1);
		button5_2 = (Button) findViewById(R.id.button5_2);
		button5_3 = (Button) findViewById(R.id.button5_3);
		button5_4 = (Button) findViewById(R.id.button5_4);
		button6_1 = (Button) findViewById(R.id.button6_1);
		button6_2 = (Button) findViewById(R.id.button6_2);
		button6_3 = (Button) findViewById(R.id.button6_3);

		button1_1.setOnClickListener(this); 
		button1_2.setOnClickListener(this); 
		button2_1.setOnClickListener(this); 
		button2_2.setOnClickListener(this); 
		button2_3.setOnClickListener(this); 
		button2_4.setOnClickListener(this); 
		button3_1.setOnClickListener(this); 
		button3_2.setOnClickListener(this); 
		button3_3.setOnClickListener(this); 
		button3_4.setOnClickListener(this); 
		button4_1.setOnClickListener(this); 
		button4_2.setOnClickListener(this); 
		button4_3.setOnClickListener(this); 
		button4_4.setOnClickListener(this); 
		button5_1.setOnClickListener(this); 
		button5_2.setOnClickListener(this); 
		button5_3.setOnClickListener(this); 
		button5_4.setOnClickListener(this); 
		button6_1.setOnClickListener(this); 
		button6_2.setOnClickListener(this); 
		button6_3.setOnClickListener(this); 

		point_interrogation.setOnClickListener(this);

		// ////////////////////////////fin gestion
		// mp////////////////////////////////

		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(2500);// 3500
		fadeOut.setDuration(1000);

		animation = new AnimationSet(true);
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);

		super.onCreate(savedInstanceState);

		imgVer.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				Random rand = new Random();
				randomInt = rand.nextInt();
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				try_stop_mp(mp);

				if ( test_1.equals("") || test_2.equals("") || test_3.equals("") ||
						test_4.equals("") || test_5.equals("") || test_6.equals("") ) 	
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_incomplet);
					mp.start();
				} else if (bloc_1 && bloc_2 && bloc_3 && bloc_4 && bloc_5 && bloc_6 ) 
				{
					Changer_Color();
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_vrai_1);
					mp.start();

				} else 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);

					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_faux_2);
					mp.start();

					Message msg = new Message();
					msg.what = STOP;
					Handler.sendMessageDelayed(msg, active);
				}



			}
		});

		home.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Modules.class));

				act.finish();

			}
		});

		exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();

			}
		});

		ex3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Ex3_m14.class));
				act.finish();

			}
		});

		ex1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Ex1_m14.class));

				act.finish();

			}
		});

		imgCorr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				imgCorr.setVisibility(View.INVISIBLE);

			}
		});

		rep.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();
				startActivity(getIntent());
			}
		});

		audio.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				mp = new MediaPlayer();
				mp = MediaPlayer.create(Act_Ex2_m14.this, R.raw.m5_m14_ex2);
				mp.start();
			}
		});


	}


	public void onClick(View v) 
	{
		switch (v.getId()) 
		{
		case R.id.point_interrogation:
			Dialog alert = new Dialog(Act_Ex2_m14.this);
			alert.requestWindowFeature(Window.FEATURE_NO_TITLE);
			alert.setContentView(R.layout.strocture_fenetre_ex2_m14);
			alert.show();
			break;
			// -1-
		case R.id.button1_1:
			button1_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button1_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_1=false;
			test_1="1";
			break;
		case R.id.button1_2:
			button1_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button1_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			bloc_1=true;
			test_1="2";
			break;
			// -2-
		case R.id.button2_1:
			button2_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button2_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button2_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button2_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_2=false;
			test_2="1";
			break;
		case R.id.button2_2:
			button2_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button2_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button2_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button2_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_2=false;
			test_2="2";
			break;
		case R.id.button2_3:
			button2_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button2_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button2_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button2_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_2=true;
			test_2="3";
			break;
		case R.id.button2_4:
			button2_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button2_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button2_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button2_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			bloc_2=false;
			test_2="4";
			break;
			// -3-
		case R.id.button3_1:
			button3_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button3_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button3_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button3_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_3=false;
			test_3="1";
			break;
		case R.id.button3_2:
			button3_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button3_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button3_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button3_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_3=false;
			test_3="2";
			break;
		case R.id.button3_3:
			button3_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button3_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button3_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button3_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_3=true;
			test_3="3";
			break;
		case R.id.button3_4:
			button3_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button3_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button3_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button3_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			bloc_3=false;
			test_3="4";
			break;
			// -4-
		case R.id.button4_1:
			button4_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button4_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button4_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button4_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_4=false;
			test_4="1";
			break;
		case R.id.button4_2:
			button4_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button4_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button4_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button4_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_4=false;
			test_4="2";
			break;
		case R.id.button4_3:
			button4_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button4_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button4_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button4_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_4=false;
			test_4="3";
			break;
		case R.id.button4_4:
			button4_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button4_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button4_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button4_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			bloc_4=true;
			test_4="4";
			break;
			// -5-
		case R.id.button5_1:
			button5_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button5_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button5_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button5_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_5=false;
			test_5="1";
			break;
		case R.id.button5_2:
			button5_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button5_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button5_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button5_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_5=false;
			test_5="2";
			break;
		case R.id.button5_3:
			button5_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button5_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button5_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button5_4.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_5=false;
			test_5="3";
			break;
		case R.id.button5_4:
			button5_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button5_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button5_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button5_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			bloc_5=true;
			test_5="4";
			break;
			// -6-
		case R.id.button6_1:
			button6_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button6_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button6_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_6=false;
			test_6="1";
			break;
		case R.id.button6_2:
			button6_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button6_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			button6_3.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			bloc_6=false;
			test_6="2";
			break;
		case R.id.button6_3:
			button6_1.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button6_2.setBackgroundColor(android.graphics.Color.TRANSPARENT);
			button6_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_initial));
			bloc_6=true;
			test_6="3";
			break;
		default:
			break;
		}
	}

	public void Changer_Color()
	{
		// 1
		if (bloc_1==false)
		{
			if (test_1.equals("1"))
			{
				button1_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
		}else
		{
			if (test_1.equals("2"))
			{
				button1_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_vert));
			}
		}
		// 2
		if (bloc_2==false)
		{
			if (test_2.equals("1"))
			{
				button2_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
			if (test_2.equals("2"))
			{
				button2_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
			if (test_2.equals("4"))
			{
				button2_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
		}else
		{
			if (test_2.equals("3"))
			{
				button2_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_vert));
			}
		}
		// 3
		if (bloc_3==false)
		{
			if (test_3.equals("1"))
			{
				button3_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
			if (test_3.equals("3"))
			{
				button3_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
			if (test_3.equals("4"))
			{
				button3_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
		}else
		{
			if (test_3.equals("3"))
			{
				button3_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_vert));
			}
		}
		// 4
		if (bloc_4==false)
		{
			if (test_4.equals("1"))
			{
				button4_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
			if (test_4.equals("2"))
			{
				button4_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
			if (test_4.equals("3"))
			{
				button4_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
		}else
		{
			if (test_4.equals("4"))
			{
				button4_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_vert));
			}
		}
		// 5
		if (bloc_5==false)
		{
			if (test_5.equals("1"))
			{
				button5_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
			if (test_5.equals("2"))
			{
				button5_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
			if (test_5.equals("3"))
			{
				button5_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
		}else
		{
			if (test_5.equals("4"))
			{
				button5_4.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_vert));
			}
		}
		// 6
		if (bloc_6==false)
		{
			if (test_6.equals("1"))
			{
				button6_1.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}
			if (test_6.equals("2"))
			{
				button6_2.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_rouge));
			}

		}else
		{
			if (test_6.equals("3"))
			{
				button6_3.setBackgroundDrawable(getResources().getDrawable(R.drawable.virgule_vert));
			}
		}

	}

	private Handler Handler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			Changer_Color();
		}		

	};

	public void try_stop_mp(MediaPlayer mp)
	{
		try {

			if (mp != null)
			{
				mp.stop();
				mp.release();
			}

		} catch (Exception e) 
		{
		}
	}

	protected void onRestart() {
		super.onRestart();
	}

	protected void onPause() {
		super.onPause();
	}
	public boolean isOnline() {

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		Scalable.scaleContents(this);
	}

}