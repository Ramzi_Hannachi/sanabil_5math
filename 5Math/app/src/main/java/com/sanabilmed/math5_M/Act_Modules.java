package com.sanabilmed.math5_M;

import otherClasses.ImageAdapter;
import utils.SanabilActivity;
import utils.Utils;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import com.sanabilmed.math5_M12.Module12;
import com.sanabilmed.math5_M14.Module14;
import com.sanabilmed.science4N.R;

public class Act_Modules extends SanabilActivity implements OnClickListener, OnItemClickListener 
{

	private ImageView  exit, home;
	private ListView listViewArticles;

	@Override
	public void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.act_modules);

		exit = (ImageView) findViewById(R.id.exit);
		exit.setOnClickListener(this);
		home = (ImageView) findViewById(R.id.home);
		home.setOnClickListener(this);

		Integer[] lListIcone={R.drawable.module_12 , R.drawable.module_14};
		ArrayAdapter<Integer> adapter = new ImageAdapter(this, R.layout.rowlv_module, lListIcone);

		listViewArticles = (ListView) findViewById(R.id.listView1);
		listViewArticles.setAdapter(adapter);
		listViewArticles.setDivider(null);

		listViewArticles.setOnItemClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.exit:
			finish();
			break;
		case R.id.home:
			Intent int_home = new Intent(this, Act_Home.class);
			startActivity(int_home);
			Utils.animateFad(this);
			finish();
			break;

		default:
			break;
		}
	}

	@Override
	public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) 
	{
		switch (position)
		{
		case 0:
			startActivity(new Intent(this, Module12.class));
			finish();			
			break;
		case 1:
			startActivity(new Intent(this, Module14.class));
			finish();
			break;
		default:
			break;
		}

	}
}
