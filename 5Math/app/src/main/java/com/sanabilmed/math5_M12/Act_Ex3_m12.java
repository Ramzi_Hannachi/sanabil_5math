package com.sanabilmed.math5_M12;

import java.util.Random;
import java.util.Vector;

import utils.Scalable;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.view.DragEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.DragShadowBuilder;
import android.view.View.OnClickListener;
import android.view.View.OnDragListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.sanabilmed.math5_M.Act_Modules;
import com.sanabilmed.science4N.R;



public class Act_Ex3_m12 extends Activity
{
	private WakeLock mWakeLock;
	private ImageView rep,ex1, ex2, audio,
	imgVer, imgCorr, imgVerEx2, exit, home;
	private MediaPlayer mp;
	protected Vector<View> images;
	AnimationSet animation;

	private ImageView prop_109 , prop_1165 , prop_863 , prop_1076 , prop_12085 , prop_87 
	, prop_11457, prop_118 , prop_8513 ;  

	private RelativeLayout zd_109, zd_1165, zd_863, zd_1076, zd_12085, zd_87 , zd_11457
	, zd_118 , zd_8513 ;

	private RelativeLayout conteneur_1, conteneur_2, conteneur_3 , conteneur_4 , conteneur_5 ,
	conteneur_6 , conteneur_7 , conteneur_8 , conteneur_9;

	private Boolean _conteneur_1 = false, _conteneur_2 = false,_conteneur_3 = false,
			_conteneur_4 = false , _conteneur_5 = false , _conteneur_6 = false, _conteneur_7 = false,
			_conteneur_8 = false, _conteneur_9 = false ;

	private int cont=0;
	private int test=0;

	private boolean scalingComplete = false;
	Activity act;
	ClipData data;
	int randomInt;

	private static final long active = 3000;
	private static final int STOP = 0;
	private Message msg;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onCreate(Bundle savedInstanceState)
	{
		setContentView(R.layout.act_ex3_m12);

		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);
		act = this;
		ex1 = (ImageView) findViewById(R.id.ex_1);
		ex2 = (ImageView) findViewById(R.id.ex_2);

		imgCorr = (ImageView) findViewById(R.id.imgCorr);
		imgVer = (ImageView) findViewById(R.id.imgVer);
		rep = (ImageView) findViewById(R.id.repeat);
		audio = (ImageView) findViewById(R.id.jouerSon);

		prop_109 =(ImageView) findViewById(R.id.obj_109);
		prop_1165 =(ImageView) findViewById(R.id.obj_1165);
		prop_863 =(ImageView) findViewById(R.id.obj_863);
		prop_1076 =(ImageView) findViewById(R.id.obj_1076);
		prop_12085 =(ImageView) findViewById(R.id.obj_12085);
		prop_87 =(ImageView) findViewById(R.id.obj_87);
		prop_11457=(ImageView) findViewById(R.id.obj_11457);
		prop_118  =(ImageView) findViewById(R.id.obj_118);
		prop_8513 =(ImageView) findViewById(R.id.obj_8513);

		zd_109 =(RelativeLayout) findViewById(R.id.zd_109);
		zd_1165 =(RelativeLayout) findViewById(R.id.zd_1165);
		zd_863 =(RelativeLayout) findViewById(R.id.zd_863);
		zd_1076 =(RelativeLayout) findViewById(R.id.zd_1076);
		zd_12085 =(RelativeLayout) findViewById(R.id.zd_12085);
		zd_87 =(RelativeLayout) findViewById(R.id.zd_87);
		zd_11457 =(RelativeLayout) findViewById(R.id.zd_11457);
		zd_118 =(RelativeLayout) findViewById(R.id.zd_118);
		zd_8513 =(RelativeLayout) findViewById(R.id.zd_8513);

		conteneur_1 = (RelativeLayout) findViewById(R.id.za_1);
		conteneur_2 = (RelativeLayout) findViewById(R.id.za_2);
		conteneur_3 = (RelativeLayout) findViewById(R.id.za_3);
		conteneur_4 = (RelativeLayout) findViewById(R.id.za_4);
		conteneur_5 = (RelativeLayout) findViewById(R.id.za_5);
		conteneur_6 = (RelativeLayout) findViewById(R.id.za_6);
		conteneur_7 = (RelativeLayout) findViewById(R.id.za_7);
		conteneur_8 = (RelativeLayout) findViewById(R.id.za_8);
		conteneur_9 = (RelativeLayout) findViewById(R.id.za_9);

		// ////////////////////////////fin gestion
		// mp////////////////////////////////

		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(2500);// 3500
		fadeOut.setDuration(1000);

		animation = new AnimationSet(true);
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);

		super.onCreate(savedInstanceState);

		imgVer.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				Random rand = new Random();
				randomInt = rand.nextInt();
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				try_stop_mp(mp);

				if (test < 9) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_incomplet);
					mp.start();
				} else if (cont == 9) 
				{
					ChangerColorImage_Vert();
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_vrai_1);
					mp.start();

				} else {
					ChangerColorImage_Vert();
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);

					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_faux_2);
					mp.start();

					Message msg = new Message();
					msg.what = STOP;
					Handler.sendMessageDelayed(msg, active);
				}



			}
		});

		home.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Modules.class));

				act.finish();

			}
		});

		exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();

			}
		});

		ex2.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Ex2_m12.class));

				act.finish();

			}
		});

		ex1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Ex1_m12.class));

				act.finish();

			}
		});

		imgCorr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				imgCorr.setVisibility(View.INVISIBLE);

			}
		});

		rep.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();
				startActivity(getIntent());
			}
		});

		audio.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				mp = new MediaPlayer();
				mp = MediaPlayer.create(Act_Ex3_m12.this, R.raw.m5_m12_ex3);
				mp.start();
			}
		});

		prop_109.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction()==MotionEvent.ACTION_DOWN)
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_109.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});

		prop_1165.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction()==MotionEvent.ACTION_DOWN)
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_1165.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_863.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction()==MotionEvent.ACTION_DOWN)
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_863.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_1076.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction()==MotionEvent.ACTION_DOWN)
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_1076.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_12085.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction()==MotionEvent.ACTION_DOWN)
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_12085.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_87.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction()==MotionEvent.ACTION_DOWN)
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_87.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_11457.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction()==MotionEvent.ACTION_DOWN)
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_11457.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_118.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction()==MotionEvent.ACTION_DOWN)
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_118.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});
		prop_8513.setOnTouchListener(new OnTouchListener() 
		{
			public boolean onTouch(View view, MotionEvent motionEvent)
			{
				if (motionEvent.getAction()==MotionEvent.ACTION_DOWN)
				{
					DragShadowBuilder shadowBuilder = new View.DragShadowBuilder(view);
					view.startDrag(data, shadowBuilder, view, 0);
					view.setVisibility(View.INVISIBLE);
					return true;
				}
				else 
				{
					prop_8513.setVisibility(View.VISIBLE);
					return false;
				}
			}
		});

		conteneur_1.setOnDragListener(new OnDragListener()
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) 
				{
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_1.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_8513 ) 
					{
						_conteneur_1 = true;
						cont++;
					}
					test++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});

		conteneur_2.setOnDragListener(new OnDragListener()
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) 
				{
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_2.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_863 ) 
					{
						_conteneur_2 = true;
						cont++;
					}
					test++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});


		conteneur_3.setOnDragListener(new OnDragListener()
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) 
				{
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_3.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_87 ) 
					{
						_conteneur_3 = true;
						cont++;
					}
					test++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});


		conteneur_4.setOnDragListener(new OnDragListener()
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) 
				{
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_4.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_1076 ) 
					{
						_conteneur_4 = true;
						cont++;
					}
					test++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});


		conteneur_5.setOnDragListener(new OnDragListener()
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) 
				{
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_5.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_109 ) 
					{
						_conteneur_5 = true;
						cont++;
					}
					test++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});


		conteneur_6.setOnDragListener(new OnDragListener()
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) 
				{
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_6.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_11457 ) 
					{
						_conteneur_6 = true;
						cont++;
					}
					test++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});

		conteneur_7.setOnDragListener(new OnDragListener()
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) 
				{
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_7.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_1165 ) 
					{
						_conteneur_7 = true;
						cont++;
					}
					test++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});

		conteneur_8.setOnDragListener(new OnDragListener()
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) 
				{
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_8.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_118 ) 
					{
						_conteneur_8 = true;
						cont++;
					}
					test++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});

		conteneur_9.setOnDragListener(new OnDragListener()
		{
			public boolean onDrag(View v, DragEvent event)
			{
				switch (event.getAction()) 
				{
				case DragEvent.ACTION_DRAG_STARTED:
					break;
				case DragEvent.ACTION_DRAG_ENTERED:
					break;
				case DragEvent.ACTION_DRAG_EXITED:
					break;
				case DragEvent.ACTION_DROP:
					View view4 = (View) event.getLocalState();
					ViewGroup owner = (ViewGroup) view4.getParent();
					owner.removeView(view4);
					RelativeLayout container = (RelativeLayout) v;
					container.addView(view4);
					view4.setVisibility(View.VISIBLE);
					conteneur_9.setEnabled(false);
					view4.setEnabled(false);
					if (view4 == prop_12085 ) 
					{
						_conteneur_9 = true;
						cont++;
					}
					test++;
					break;
				case DragEvent.ACTION_DRAG_ENDED:
				default:
					break;
				}
				return true;
			}
		});

	}

	public void ChangerColorImage_Vert()
	{
		// conteneur 1
		if (_conteneur_1==true)
		{
			View v1 = conteneur_1.getChildAt(0);
			if (v1==prop_8513) 
			{
				conteneur_1.setBackgroundResource(R.drawable.m5_mo13_ex3_ic1_vrai);
			}
			conteneur_1.removeView(v1);
		}else
		{
			ChangerViewContenair(conteneur_1);
		}
		// conteneur 2
		if (_conteneur_2==true)
		{
			View v2 = conteneur_2.getChildAt(0);
			if (v2==prop_863) 
			{
				conteneur_2.setBackgroundResource(R.drawable.m5_mo13_ex3_ic7_vrai);
			}
			conteneur_2.removeView(v2);
		}else
		{
			ChangerViewContenair(conteneur_2);
		}
		// conteneur 3
		if (_conteneur_3==true)
		{
			View v3 = conteneur_3.getChildAt(0);
			if (v3==prop_87) 
			{
				conteneur_3.setBackgroundResource(R.drawable.m5_mo13_ex3_ic4_vrai);
			}
			conteneur_3.removeView(v3);
		}else
		{
			ChangerViewContenair(conteneur_3);
		}
		// conteneur 4
		if (_conteneur_4==true)
		{
			View v4 = conteneur_4.getChildAt(0);
			if (v4==prop_1076) 
			{
				conteneur_4.setBackgroundResource(R.drawable.m5_mo13_ex3_ic6_vrai);
			}
			conteneur_4.removeView(v4);
		}else
		{
			ChangerViewContenair(conteneur_4);
		}
		// conteneur 5
		if (_conteneur_5==true)
		{
			View v5 = conteneur_5.getChildAt(0);
			if (v5==prop_109) 
			{
				conteneur_5.setBackgroundResource(R.drawable.m5_mo13_ex3_ic9_vrai);
			}
			conteneur_5.removeView(v5);
		}else
		{
			ChangerViewContenair(conteneur_5);
		}
		// conteneur 6
		if (_conteneur_6==true)
		{
			View v6 = conteneur_6.getChildAt(0);
			if (v6==prop_11457) 
			{
				conteneur_6.setBackgroundResource(R.drawable.m5_mo13_ex3_ic3_vrai);
			}
			conteneur_6.removeView(v6);
		}else
		{
			ChangerViewContenair(conteneur_6);
		}
		// conteneur 7
		if (_conteneur_7==true)
		{
			View v7 = conteneur_7.getChildAt(0);
			if (v7==prop_1165) 
			{
				conteneur_7.setBackgroundResource(R.drawable.m5_mo13_ex3_ic8_vrai);
			}
			conteneur_7.removeView(v7);
		}else
		{
			ChangerViewContenair(conteneur_7);
		}
		// conteneur 8
		if (_conteneur_8==true)
		{
			View v8 = conteneur_8.getChildAt(0);
			if (v8==prop_118) 
			{
				conteneur_8.setBackgroundResource(R.drawable.m5_mo13_ex3_ic2_vrai);
			}
			conteneur_8.removeView(v8);
		}else
		{
			ChangerViewContenair(conteneur_8);
		}
		// conteneur 9
		if (_conteneur_9==true)
		{
			View v9 = conteneur_9.getChildAt(0);
			if (v9==prop_12085) 
			{
				conteneur_9.setBackgroundResource(R.drawable.m5_mo13_ex3_ic5_vrai);
			}
			conteneur_9.removeView(v9);
		}else
		{
			ChangerViewContenair(conteneur_9);
		}

	}

	public void ChangerViewContenair(RelativeLayout contenaire)
	{
		View v1 = contenaire.getChildAt(0); 
		if (v1==prop_8513)
		{
			contenaire.removeView(prop_8513);
			prop_8513.setImageResource(R.drawable.m5_mo13_ex3_ic1_faux);
			contenaire.addView(prop_8513);
		}
		if (v1==prop_118)
		{
			contenaire.removeView(prop_118);
			prop_118.setImageResource(R.drawable.m5_mo13_ex3_ic2_faux);
			contenaire.addView(prop_118);
		}
		if (v1==prop_11457)
		{
			contenaire.removeView(prop_11457);
			prop_11457.setImageResource(R.drawable.m5_mo13_ex3_ic3_faux);
			contenaire.addView(prop_11457);
		}
		if (v1==prop_87)
		{
			contenaire.removeView(prop_87);
			prop_87.setImageResource(R.drawable.m5_mo13_ex3_ic4_faux);
			contenaire.addView(prop_87);
		}if (v1==prop_12085)
		{
			contenaire.removeView(prop_12085);
			prop_12085.setImageResource(R.drawable.m5_mo13_ex3_ic5_faux);
			contenaire.addView(prop_12085);
		}
		if (v1==prop_1076)
		{
			contenaire.removeView(prop_1076);
			prop_1076.setImageResource(R.drawable.m5_mo13_ex3_ic6_faux);
			contenaire.addView(prop_1076);
		}
		if (v1==prop_863)
		{
			contenaire.removeView(prop_863);
			prop_863.setImageResource(R.drawable.m5_mo13_ex3_ic7_faux);
			contenaire.addView(prop_863);
		}
		if (v1==prop_1165)
		{
			contenaire.removeView(prop_1165);
			prop_1165.setImageResource(R.drawable.m5_mo13_ex3_ic8_faux);
			contenaire.addView(prop_1165);
		}
		if (v1==prop_109)
		{
			contenaire.removeView(prop_109);
			prop_109.setImageResource(R.drawable.m5_mo13_ex3_ic9_faux);
			contenaire.addView(prop_109);
		}
	}

	private Handler Handler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			if (!_conteneur_1 ) 
			{
				View v1 = conteneur_1.getChildAt(0);
				conteneur_1.removeView(v1);
				resetImage(v1);
				v1.setVisibility(View.VISIBLE);
				test--;
				conteneur_1.setEnabled(true);
				v1.setEnabled(true);
			}
			if (!_conteneur_2 ) 
			{
				View v2 = conteneur_2.getChildAt(0);
				conteneur_2.removeView(v2);
				resetImage(v2);
				v2.setVisibility(View.VISIBLE);
				test--;
				conteneur_2.setEnabled(true);
				v2.setEnabled(true);
			}
			if (!_conteneur_3 ) 
			{
				View v3 = conteneur_3.getChildAt(0);
				conteneur_3.removeView(v3);
				resetImage(v3);
				v3.setVisibility(View.VISIBLE);
				test--;
				conteneur_3.setEnabled(true);
				v3.setEnabled(true);
			}
			if (!_conteneur_4 ) 
			{
				View v4 = conteneur_4.getChildAt(0);
				conteneur_4.removeView(v4);
				resetImage(v4);
				v4.setVisibility(View.VISIBLE);
				test--;
				conteneur_4.setEnabled(true);
				v4.setEnabled(true);
			}
			if (!_conteneur_5 ) 
			{
				View v5 = conteneur_5.getChildAt(0);
				conteneur_5.removeView(v5);
				resetImage(v5);
				v5.setVisibility(View.VISIBLE);
				test--;
				conteneur_5.setEnabled(true);
				v5.setEnabled(true);
			}
			if (!_conteneur_6 ) 
			{
				View v6 = conteneur_6.getChildAt(0);
				conteneur_6.removeView(v6);
				resetImage(v6);
				v6.setVisibility(View.VISIBLE);
				test--;
				conteneur_6.setEnabled(true);
				v6.setEnabled(true);
			}
			if (!_conteneur_7 ) 
			{
				View v7 = conteneur_7.getChildAt(0);
				conteneur_7.removeView(v7);
				resetImage(v7);
				v7.setVisibility(View.VISIBLE);
				test--;
				conteneur_7.setEnabled(true);
				v7.setEnabled(true);
			}
			if (!_conteneur_8 ) 
			{
				View v8 = conteneur_8.getChildAt(0);
				conteneur_8.removeView(v8);
				resetImage(v8);
				v8.setVisibility(View.VISIBLE);
				test--;
				conteneur_8.setEnabled(true);
				v8.setEnabled(true);
			}
			if (!_conteneur_9 ) 
			{
				View v9 = conteneur_9.getChildAt(0);
				conteneur_9.removeView(v9);
				resetImage(v9);
				v9.setVisibility(View.VISIBLE);
				test--;
				conteneur_9.setEnabled(true);
				v9.setEnabled(true);
			}


			super.handleMessage(msg);
		}

	};

	public void resetImage(View v) 
	{
		if (v == prop_109 )
		{
			prop_109.setImageResource(R.drawable.m5_mo13_ex3_ic9_initial);
			zd_109.addView(v);
		}
		if (v == prop_1165)
		{
			prop_1165.setImageResource(R.drawable.m5_mo13_ex3_ic8_initial);
			zd_1165.addView(v);
		}
		if (v == prop_863) 
		{
			prop_863.setImageResource(R.drawable.m5_mo13_ex3_ic7_initial);
			zd_863.addView(v);
		}
		if (v == prop_1076) 
		{
			prop_1076.setImageResource(R.drawable.m5_mo13_ex3_ic6_initial);
			zd_1076.addView(v);
		}
		if (v == prop_12085 )
		{
			prop_12085.setImageResource(R.drawable.m5_mo13_ex3_ic5_initial);
			zd_12085.addView(v);
		}
		if (v == prop_87)
		{
			prop_87.setImageResource(R.drawable.m5_mo13_ex3_ic4_initial);
			zd_87.addView(v);
		}
		if (v == prop_11457 )
		{
			prop_11457.setImageResource(R.drawable.m5_mo13_ex3_ic3_initial);
			zd_11457.addView(v);
		}
		if (v == prop_118 )
		{
			prop_118.setImageResource(R.drawable.m5_mo13_ex3_ic2_initial);
			zd_118.addView(v);
		}
		if (v == prop_8513 )
		{
			prop_8513.setImageResource(R.drawable.m5_mo13_ex3_ic1_initial);
			zd_8513.addView(v);
		}
	}

	public void try_stop_mp(MediaPlayer mp) {
		try {

			if (mp != null) {
				mp.stop();
				mp.release();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	protected void onRestart() {
		super.onRestart();
	}

	protected void onPause() {
		super.onPause();
	}
	public boolean isOnline() {

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		Scalable.scaleContents(this);
	}

}