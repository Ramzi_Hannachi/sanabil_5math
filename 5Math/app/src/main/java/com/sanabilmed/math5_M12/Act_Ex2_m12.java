package com.sanabilmed.math5_M12;

import java.util.Random;
import java.util.Vector;

import utils.Scalable;
import android.annotation.TargetApi;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.PowerManager.WakeLock;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;

import com.sanabilmed.math5_M.Act_Modules;
import com.sanabilmed.science4N.R;
public class Act_Ex2_m12 extends Activity implements OnClickListener
{
	private WakeLock mWakeLock;
	private ImageView rep, ex3,ex1, audio,
	imgVer, imgCorr, imgVerEx2, exit, home;
	private MediaPlayer mp;
	protected Vector<View> images;
	AnimationSet animation;

	private ImageView image_031_75 , image_04_75 , image_07_100 , image_007_84 , image_0032_372 ,
	image_004_372 , image_095_38 , image_085_13 , image_086_56 , image_063_56 , image_01_103 , 
	image_005_103 ;

	private String bloc_1="" , bloc_2="" , bloc_3="" 
			, bloc_4="" , bloc_5="" , bloc_6="";

	private boolean scalingComplete = false;
	Activity act;
	ClipData data;
	int randomInt;

	private static final long active = 2000;
	private static final int STOP = 0;
	private Message msg;

	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	public void onCreate(Bundle savedInstanceState)
	{
		setContentView(R.layout.act_ex2_m12);
		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);

		act = this;

		ex1 = (ImageView) findViewById(R.id.ex_1);
		ex3 = (ImageView) findViewById(R.id.ex_3);

		imgCorr = (ImageView) findViewById(R.id.imgCorr);
		imgVer = (ImageView) findViewById(R.id.imgVer);
		rep = (ImageView) findViewById(R.id.repeat);
		audio = (ImageView) findViewById(R.id.jouerSon);

		image_031_75 =(ImageView) findViewById(R.id.image_031_75);
		image_04_75 =(ImageView) findViewById(R.id.image_04_75); 
		image_07_100 =(ImageView) findViewById(R.id.image_07_100);
		image_007_84 =(ImageView) findViewById(R.id.image_007_84);
		image_0032_372 =(ImageView) findViewById(R.id.image_0032_372);
		image_004_372 =(ImageView) findViewById(R.id.image_004_372);
		image_095_38 =(ImageView) findViewById(R.id.image_095_38);
		image_085_13 =(ImageView) findViewById(R.id.image_085_13);
		image_086_56 =(ImageView) findViewById(R.id.image_086_56);
		image_063_56 =(ImageView) findViewById(R.id.image_063_56);
		image_01_103 =(ImageView) findViewById(R.id.image_01_103);
		image_005_103 =(ImageView) findViewById(R.id.image_005_103);

		// ////////////////////////////fin gestion
		// mp////////////////////////////////

		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(2500);// 3500
		fadeOut.setDuration(1000);

		animation = new AnimationSet(true); // change to false
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);

		super.onCreate(savedInstanceState);

		image_031_75.setOnClickListener(this);
		image_04_75.setOnClickListener(this); 
		image_07_100.setOnClickListener(this);
		image_007_84.setOnClickListener(this);
		image_0032_372.setOnClickListener(this);
		image_004_372.setOnClickListener(this);
		image_095_38.setOnClickListener(this);
		image_085_13.setOnClickListener(this);
		image_086_56.setOnClickListener(this);
		image_063_56.setOnClickListener(this);
		image_01_103.setOnClickListener(this);
		image_005_103.setOnClickListener(this);

		imgVer.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				Random rand = new Random();
				randomInt = rand.nextInt();
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				try_stop_mp(mp);

				if ( Verifier_test()==false ) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_incomplet);
					mp.start();
				} else if ( Verifier_Repense() ) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_vrai_1);
					mp.start();

				} else {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);

					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_faux_2);
					mp.start();
					Color_Changer();

					Message msg = new Message();
					msg.what = STOP;
					Handler.sendMessageDelayed(msg, active);
				}


			}
		});

		home.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Modules.class));

				act.finish();

			}
		});
		exit.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();

			}
		});

		ex1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Ex1_m12.class));

				act.finish();

			}
		});

		ex3.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Ex3_m12.class));

				act.finish();

			}
		});

		imgCorr.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {

				imgCorr.setVisibility(View.INVISIBLE);

			}
		});

		rep.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();
				startActivity(getIntent());
			}
		});

		audio.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				try_stop_mp(mp);
				mp = new MediaPlayer();
				mp = MediaPlayer.create(Act_Ex2_m12.this, R.raw.m5_m12_ex2);
				mp.start();
			}
		});

	}

	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.image_031_75:
			bloc_1="031_75";
			image_031_75.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic5_select));
			image_04_75.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic6_initial));
			break;
		case R.id.image_04_75:
			bloc_1="04_75";
			image_04_75.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic6_select));
			image_031_75.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic5_initial));
			break;
		case R.id.image_07_100:
			bloc_2="07_100";
			image_07_100.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic3_select));
			image_007_84.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic4_initial));
			break;
		case R.id.image_007_84:
			bloc_2="007_84";
			image_007_84.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic4_select));
			image_07_100.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic3_initial));
			break;
		case R.id.image_0032_372:
			bloc_3="0032_372";
			image_0032_372.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic1_select));
			image_004_372.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic2_initial));
			break;
		case R.id.image_004_372:
			bloc_3="004_372";
			image_004_372.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic2_select));
			image_0032_372.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic1_initial));
			break;
		case R.id.image_095_38:
			bloc_4="095_38";
			image_095_38.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic11_select));
			image_085_13.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic12_initial));
			break;
		case R.id.image_085_13:
			bloc_4="085_13";
			image_085_13.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic12_select));
			image_095_38.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic11_initial));
			break;
		case R.id.image_086_56:
			bloc_5="086_56";
			image_086_56.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic9_select));
			image_063_56.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic10_initial));
			break;
		case R.id.image_063_56:
			bloc_5="063_56";
			image_063_56.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic10_select));
			image_086_56.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic9_initial));
			break;
		case R.id.image_01_103:
			bloc_6="01_103";
			image_01_103.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic7_select));
			image_005_103.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic8_initial));
			break;
		case R.id.image_005_103:
			bloc_6="005_103";
			image_005_103.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic8_select));
			image_01_103.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic7_initial));
			break;
		default:
			break;
		}
	}

	public boolean Verifier_Repense()
	{
		if ( bloc_1.equals("04_75") && bloc_2.equals("07_100") && bloc_3.equals("004_372") && 
				bloc_4.equals("095_38") && bloc_5.equals("086_56") && bloc_6.equals("01_103") ) 
		{
			image_04_75.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic6_vrai));
			image_07_100.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic3_vrai));
			image_004_372.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic2_vrai));
			image_095_38.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic11_vrai));
			image_086_56.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic9_vrai));
			image_01_103.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic7_vrai));
			return true;
		}else
		{
			return false;
		}
	}

	public boolean Verifier_test()
	{
		if (!bloc_1.equals("") && !bloc_2.equals("") && !bloc_3.equals("") && 
				!bloc_4.equals("") && !bloc_5.equals("") && !bloc_6.equals(""))
		{
			return true;
		}else
		{
			return false;
		}
	}

	public void Color_Changer()
	{
		// bloc_1
		if (bloc_1.equals("04_75"))
		{
			image_04_75.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic6_vrai));
		}else
		{
			image_031_75.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic5_faux));
		}
		// bloc_2
		if (bloc_2.equals("07_100")) 
		{
			image_07_100.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic3_vrai));
		}else
		{
			image_007_84.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic4_faux));
		}
		// bloc_3
		if (bloc_3.equals("004_372")) 
		{
			image_004_372.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic2_vrai));
		}else
		{
			image_0032_372.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic1_faux));
		}
		// bloc_4
		if (bloc_4.equals("095_38")) 
		{
			image_095_38.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic11_vrai));
		}else
		{
			image_085_13.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic12_faux));
		}
		// bloc_5
		if (bloc_5.equals("086_56")) 
		{
			image_086_56.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic9_vrai));
		}else
		{
			image_063_56.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic10_faux));
		}
		// bloc_6
		if (bloc_6.equals("01_103")) 
		{
			image_01_103.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic7_vrai));
		}else
		{
			image_005_103.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic8_faux));
		}

	}

	private Handler Handler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{
			// bloc_1
			if (!bloc_1.equals("04_75"))
			{
				image_031_75.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic5_initial));
				bloc_1="";
			}
			// bloc_2
			if (!bloc_2.equals("07_100")) 
			{
				image_007_84.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic4_initial));
				bloc_2="";
			}
			// bloc_3
			if (!bloc_3.equals("004_372")) 
			{
				image_0032_372.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic1_initial));
				bloc_3="";
			}
			// bloc_4
			if (!bloc_4.equals("095_38")) 
			{
				image_085_13.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic12_initial));
				bloc_4="";
			}
			// bloc_5
			if (!bloc_5.equals("086_56")) 
			{
				image_063_56.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic10_initial));
				bloc_5="";
			}
			// bloc_6
			if (!bloc_6.equals("01_103")) 
			{
				image_005_103.setImageDrawable(getResources().getDrawable(R.drawable.m6_mo11_ex2_a_ic8_initial));
				bloc_6="";
			}

			super.handleMessage(msg);
		}

	};

	public void try_stop_mp(MediaPlayer mp) 
	{
		try {

			if (mp != null) {
				mp.stop();
				mp.release();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	protected void onRestart() {
		super.onRestart();
	}

	protected void onPause() {
		super.onPause();
	}
	public boolean isOnline() {

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}

	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		Scalable.scaleContents(this);
	}
}