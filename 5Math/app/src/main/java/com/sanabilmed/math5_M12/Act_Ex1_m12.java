package com.sanabilmed.math5_M12;

import java.util.Random;

import utils.Scalable;
import android.app.Activity;
import android.content.ClipData;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable.ConstantState;
import android.media.MediaPlayer;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.AnimationSet;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;

import com.sanabilmed.math5_M.Act_Modules;
import com.sanabilmed.science4N.R;


public class Act_Ex1_m12 extends Activity implements OnClickListener
{
	private ImageView rep, audio,imgVer, imgCorr, exit, home,
	image_33_075 , image_30005_307 , image_138_706 , image_208_706 
	, image_375_10 , image_2075_13 , image_3375 , image_1375 , image_386_17 ,
	image_20075_13   , image_786_20   , 
	image_87_505 , image_2086 , image_27_086 , image_0075_33 , 	couleur_palette , ex2 , ex3 ;

	private Button palette_jaune , palette_vert , palette_rouge , palette_orange , palette_bleau;

	private String couleur_actuelle="blanc";

	private boolean boolean_33_075=false , boolean_30005_307=false , boolean_138_706=false ,
			boolean_208_706=false , boolean_375_10=false , boolean_2075_13=false , boolean_3375=false , boolean_1375=false ,
			boolean_386_17=false ,boolean_20075_13=false   , boolean_786_20=false   , 
			boolean_87_505=false , boolean_2086=false , boolean_27_086=false , boolean_0075_33=false;

	private MediaPlayer mp;
	AnimationSet animation;

	private static final long active = 2000;
	private static final int STOP = 0;
	private Message msg;

	Activity act;
	ClipData data;
	int randomInt;

	public void onCreate(Bundle savedInstanceState) 
	{
		setContentView(R.layout.act_ex1_m12);
		exit = (ImageView) findViewById(R.id.exit);
		home = (ImageView) findViewById(R.id.home);

		act = this;

		imgCorr = (ImageView) findViewById(R.id.imgCorr);
		imgVer = (ImageView) findViewById(R.id.imgVer);
		rep = (ImageView) findViewById(R.id.repeat);
		audio = (ImageView) findViewById(R.id.jouerSon);

		ex2 = (ImageView) findViewById(R.id.ex_2);
		ex3 = (ImageView) findViewById(R.id.ex_3);

		image_33_075 = (ImageView) findViewById(R.id.image_33_075);
		image_30005_307 = (ImageView) findViewById(R.id.image_30005_307);
		image_138_706 = (ImageView) findViewById(R.id.image_138_706);
		image_208_706 = (ImageView) findViewById(R.id.image_208_706);
		image_375_10 = (ImageView) findViewById(R.id.image_375_10);
		image_2075_13 = (ImageView) findViewById(R.id.image_2075_13);
		image_3375 = (ImageView) findViewById(R.id.image_3375);
		image_1375 = (ImageView) findViewById(R.id.image_1375);
		image_386_17 = (ImageView) findViewById(R.id.image_386_17);
		image_20075_13 = (ImageView) findViewById(R.id.image_20075_13);
		image_786_20 = (ImageView) findViewById(R.id.image_786_20);
		image_87_505 = (ImageView) findViewById(R.id.image_87_505);
		image_2086 = (ImageView) findViewById(R.id.image_2086);
		image_27_086 = (ImageView) findViewById(R.id.image_27_086);
		image_0075_33 = (ImageView) findViewById(R.id.image_0075_33);

		palette_jaune  =  (Button) findViewById(R.id.palette_jaune);
		palette_vert  =  (Button) findViewById(R.id.palette_vert);
		palette_rouge  =  (Button) findViewById(R.id.palette_rouge);
		palette_orange  =  (Button) findViewById(R.id.palette_orange);
		palette_bleau  = (Button) findViewById(R.id.palette_bleau);

		couleur_palette= (ImageView) findViewById(R.id.couleur_palette);

		Reset_Image();
		// ////////////////////////////fin gestion
		// mp////////////////////////////////

		Animation fadeIn = new AlphaAnimation(0, 1);
		fadeIn.setInterpolator(new DecelerateInterpolator()); // add this
		fadeIn.setDuration(1000);

		Animation fadeOut = new AlphaAnimation(1, 0);
		fadeOut.setInterpolator(new AccelerateInterpolator()); // and this
		fadeOut.setStartOffset(2500);// 3500
		fadeOut.setDuration(1000);

		animation = new AnimationSet(true); // change to false
		animation.addAnimation(fadeIn);
		animation.addAnimation(fadeOut);

		super.onCreate(savedInstanceState);

		palette_jaune.setOnClickListener(this);
		palette_vert.setOnClickListener(this);
		palette_rouge.setOnClickListener(this);
		palette_orange.setOnClickListener(this);
		palette_bleau.setOnClickListener(this);

		image_33_075.setOnClickListener(this);
		image_30005_307.setOnClickListener(this);
		image_138_706.setOnClickListener(this);
		image_208_706 .setOnClickListener(this);
		image_375_10 .setOnClickListener(this);
		image_2075_13 .setOnClickListener(this);
		image_3375 .setOnClickListener(this);
		image_1375 .setOnClickListener(this);
		image_386_17 .setOnClickListener(this);
		image_20075_13 .setOnClickListener(this);
		image_786_20 .setOnClickListener(this);
		image_87_505 .setOnClickListener(this);
		image_2086 .setOnClickListener(this);
		image_27_086 .setOnClickListener(this);
		image_0075_33 .setOnClickListener(this);

		imgVer.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v)
			{
				Random rand = new Random();
				randomInt = rand.nextInt();
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				imgCorr.setBackgroundDrawable(null);
				try_stop_mp(mp);

				if (test_Verifier()==false) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_incomplet);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),R.raw.mascotte_incomplet);
					mp.start();
				} else if (test_boolean()==true) 
				{
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_vrai_1);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_vrai_1);
					mp.start();

				} else {
					imgCorr.setVisibility(1);
					imgCorr.setBackgroundResource(R.drawable.mascotte_faux_2);
					imgCorr.startAnimation(animation);
					imgCorr.setVisibility(View.INVISIBLE);
					mp = MediaPlayer.create(getApplicationContext(),
							R.raw.mascotte_faux_2);
					mp.start();

					Message msg = new Message();
					msg.what = STOP;
					Handler.sendMessageDelayed(msg, active);
				}

			}
		});


		home.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v) {

				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(),Act_Modules.class));

				act.finish();

			}
		});
		exit.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();
			}
		});


		imgCorr.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v) 
			{
				imgCorr.setVisibility(View.INVISIBLE);
			}
		});

		rep.setOnClickListener(new OnClickListener()
		{
			public void onClick(View v) {
				try_stop_mp(mp);
				finish();
				startActivity(getIntent());
			}
		});

		audio.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v)
			{
				try_stop_mp(mp);
				mp = new MediaPlayer();
				mp = MediaPlayer.create(Act_Ex1_m12.this, R.raw.m5_m12_ex1);
				mp.start();
			}
		});

		ex2.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v)
			{
				try_stop_mp(mp);
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Ex2_m12.class));
				act.finish();
			}
		});

		ex3.setOnClickListener(new OnClickListener() 
		{
			public void onClick(View v)
			{
				try_stop_mp(mp);
				try_stop_mp(mp);
				startActivity(new Intent(getApplicationContext(), Act_Ex3_m12.class));
				act.finish();
			}
		});

	}

	public void onClick(View v) 
	{
		switch (v.getId())
		{
		case R.id.palette_jaune:
			couleur_palette.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_ic4_couleur_jaune));
			couleur_actuelle="jaune";
			break;
		case R.id.palette_vert:
			couleur_palette.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_ic1_couleur_vert));
			couleur_actuelle="vert";

			break;
		case R.id.palette_rouge:
			couleur_palette.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_ic1_couleur_rouge));
			couleur_actuelle="rouge";

			break;
		case R.id.palette_orange:
			couleur_palette.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_ic2_couleur_orange));
			couleur_actuelle="orange";

			break;
		case R.id.palette_bleau:
			couleur_palette.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_ic3_couleur_bleu));
			couleur_actuelle="bleau";

			break;
		case R.id.image_33_075:
			if (couleur_actuelle.equals("jaune"))
			{
				image_33_075.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_4_jaune));
				boolean_33_075=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_33_075.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_4_vert));
				boolean_33_075=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_33_075.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_4_rouge));
				boolean_33_075=true;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_33_075.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_4_orange));
				boolean_33_075=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_33_075.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_4_bleu));
				boolean_33_075=false;
			}
			break;

		case R.id.image_30005_307:
			if (couleur_actuelle.equals("jaune"))
			{
				image_30005_307.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_8_jaune));
				boolean_30005_307=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_30005_307.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_8_vert));
				boolean_30005_307=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_30005_307.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_8_rouge));
				boolean_30005_307=true;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_30005_307.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_8_orange));
				boolean_30005_307=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_30005_307.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_8_bleu));
				boolean_30005_307=false;
			}
			break;

		case R.id.image_138_706:
			if (couleur_actuelle.equals("jaune"))
			{
				image_138_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_14_jaune));
				boolean_138_706=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_138_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_14_vert));
				boolean_138_706=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_138_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_14_rouge));
				boolean_138_706=false;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_138_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_14_orange));
				boolean_138_706=true;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_138_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_14_bleu));
				boolean_138_706=false;
			}
			break;

		case R.id.image_208_706:
			if (couleur_actuelle.equals("jaune"))
			{
				image_208_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_19_jaune));
				boolean_208_706=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_208_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_19_vert));
				boolean_208_706=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_208_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_19_rouge));
				boolean_208_706=false;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_208_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_19_orange));
				boolean_208_706=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_208_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_19_bleu));
				boolean_208_706=true;
			}
			break;


		case R.id.image_375_10:
			if (couleur_actuelle.equals("jaune"))
			{
				image_375_10.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_15_jaune));
				boolean_375_10=true;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_375_10.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_15_vert));
				boolean_375_10=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_375_10.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_15_rouge));
				boolean_375_10=false;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_375_10.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_15_orange));
				boolean_375_10=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_375_10.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_15_bleu));
				boolean_375_10=false;
			}
			break;

		case R.id.image_2075_13:
			if (couleur_actuelle.equals("jaune"))
			{
				image_2075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_20_jaune));
				boolean_2075_13=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_2075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_20_vert));
				boolean_2075_13=true;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_2075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_20_rouge));
				boolean_2075_13=false;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_2075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_20_orange));
				boolean_2075_13=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_2075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_20_bleu));
				boolean_2075_13=false;
			}
			break;

		case R.id.image_3375:
			if (couleur_actuelle.equals("jaune"))
			{
				image_3375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_2_jaune));
				boolean_3375=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_3375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_2_vert));
				boolean_3375=true;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_3375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_2_rouge));
				boolean_3375=false;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_3375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_2_orange));
				boolean_3375=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_3375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_2_bleu));
				boolean_3375=false;
			}
			break;

		case R.id.image_1375: 
			if (couleur_actuelle.equals("jaune"))
			{
				image_1375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_7_jaune));
				boolean_1375=true;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_1375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_7_vert));
				boolean_1375=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_1375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_7_rouge));
				boolean_1375=false;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_1375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_7_orange));
				boolean_1375=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_1375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_7_bleu));
				boolean_1375=false;
			}
			break;

		case R.id.image_386_17:
			if (couleur_actuelle.equals("jaune"))
			{
				image_386_17.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_13_jaune));
				boolean_386_17=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_386_17.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_13_vert));
				boolean_386_17=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_386_17.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_13_rouge));
				boolean_386_17=false;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_386_17.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_13_orange));
				boolean_386_17=true;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_386_17.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_13_bleu));
				boolean_386_17=false;
			}
			break;

		case R.id.image_20075_13:
			if (couleur_actuelle.equals("jaune"))
			{
				image_20075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_18_jaune));
				boolean_20075_13=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_20075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_18_vert));
				boolean_20075_13=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_20075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_18_rouge));
				boolean_20075_13=true;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_20075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_18_orange));
				boolean_20075_13=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_20075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_18_bleu));
				boolean_20075_13=false;
			}
			break;

		case R.id.image_786_20:
			if (couleur_actuelle.equals("jaune"))
			{
				image_786_20.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_6_jaune));
				boolean_786_20=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_786_20.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_6_vert));
				boolean_786_20=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_786_20.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_6_rouge));
				boolean_786_20=false;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_786_20.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_6_orange));
				boolean_786_20=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_786_20.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_6_bleu));
				boolean_786_20=true;
			}
			break;

		case R.id.image_87_505:
			if (couleur_actuelle.equals("jaune"))
			{
				image_87_505.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_5_jaune));
				boolean_87_505=true;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_87_505.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_5_vert));
				boolean_87_505=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_87_505.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_5_rouge));
				boolean_87_505=false;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_87_505.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_5_orange));
				boolean_87_505=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_87_505.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_5_bleu));
				boolean_87_505=false;
			}
			break;

		case R.id.image_2086:
			if (couleur_actuelle.equals("jaune"))
			{
				image_2086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_11_jaune));
				boolean_2086=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_2086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_11_vert));
				boolean_2086=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_2086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_11_rouge));
				boolean_2086=false;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_2086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_11_orange));
				boolean_2086=true;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_2086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_11_bleu));
				boolean_2086=false;
			}
			break;


		case R.id.image_27_086:
			if (couleur_actuelle.equals("jaune"))
			{
				image_27_086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_16_jaune));
				boolean_27_086=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_27_086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_16_vert));
				boolean_27_086=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_27_086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_16_rouge));
				boolean_27_086=false;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_27_086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_16_orange));
				boolean_27_086=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_27_086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_16_bleu));
				boolean_27_086=true;
			}
			break;

		case R.id.image_0075_33:
			if (couleur_actuelle.equals("jaune"))
			{
				image_0075_33.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_10_jaune));
				boolean_0075_33=false;
			}
			if (couleur_actuelle.equals("vert"))
			{
				image_0075_33.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_10_vert));
				boolean_0075_33=false;
			}
			if (couleur_actuelle.equals("rouge"))
			{
				image_0075_33.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_10_rouge));
				boolean_0075_33=true;
			}
			if (couleur_actuelle.equals("orange"))
			{
				image_0075_33.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_10_orange));
				boolean_0075_33=false;
			}
			if (couleur_actuelle.equals("bleau"))
			{
				image_0075_33.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_10_bleu));
				boolean_0075_33=false;
			}
			break;

		default:
			break;
		}
	}


	private Handler Handler = new Handler() 
	{
		public void handleMessage(Message msg) 
		{

			if (!boolean_30005_307)
			{
				image_30005_307.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_8_vide));
			}
			if (!boolean_138_706)
			{
				image_138_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_14_vide));
			}
			if (!boolean_208_706)
			{
				image_208_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_19_vide));
			}
			if (!boolean_375_10)
			{
				image_375_10.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_15_vide));
			}
			if (!boolean_2075_13)
			{
				image_2075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_20_vide));
			}
			if (!boolean_3375)
			{
				image_3375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_2_vide));
			}
			if (!boolean_1375)
			{
				image_1375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_7_vide));
			}
			if (!boolean_386_17)
			{
				image_386_17.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_13_vide));
			}
			if (!boolean_20075_13)
			{
				image_20075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_18_vide));
			}
			if (!boolean_786_20)
			{
				image_786_20.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_6_vide));
			}
			if (!boolean_87_505)
			{
				image_87_505.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_5_vide));
			}
			if (!boolean_2086)
			{
				image_2086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_11_vide));
			}
			if (!boolean_27_086)
			{
				image_27_086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_16_vide));
			}
			if (!boolean_0075_33)
			{
				image_0075_33.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_10_vide));
			}
			if (!boolean_33_075)
			{
				image_33_075.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_4_vide));
			}
			super.handleMessage(msg);
		}

	};


	public void Reset_Image()
	{
		image_30005_307.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_8_vide));
		image_138_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_14_vide));
		image_208_706.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_19_vide));
		image_375_10.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_15_vide));
		image_2075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_20_vide));
		image_3375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_2_vide));
		image_1375.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_7_vide));
		image_386_17.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_13_vide));
		image_20075_13.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_18_vide));
		image_786_20.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_6_vide));
		image_87_505.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_5_vide));
		image_2086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_11_vide));
		image_27_086.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_16_vide));
		image_0075_33.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_10_vide));
		image_33_075.setImageDrawable(getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_4_vide));
	}

	public boolean test_Verifier()
	{
		ConstantState image_30005_307_Initiale = image_30005_307.getDrawable().getConstantState();
		ConstantState image_138_706_Initial= image_138_706.getDrawable().getConstantState();
		ConstantState image_208_706_Initial= image_208_706.getDrawable().getConstantState();
		ConstantState image_375_10_Initial= image_375_10.getDrawable().getConstantState();
		ConstantState image_2075_13_Initial=image_2075_13.getDrawable().getConstantState();
		ConstantState image_3375_Initial=image_3375.getDrawable().getConstantState();
		ConstantState image_1375_Initial=image_1375.getDrawable().getConstantState();
		ConstantState image_386_17_Initial=image_386_17.getDrawable().getConstantState();
		ConstantState image_20075_13_Initial=image_20075_13.getDrawable().getConstantState();
		ConstantState image_786_20_Initial=image_786_20.getDrawable().getConstantState();
		ConstantState image_87_505_Initial=image_87_505.getDrawable().getConstantState();
		ConstantState image_2086_Initial=image_2086.getDrawable().getConstantState();
		ConstantState image_27_086_Initial=image_27_086.getDrawable().getConstantState();
		ConstantState image_0075_33_Initial=image_0075_33.getDrawable().getConstantState();
		ConstantState image_33_075_Initial=image_33_075.getDrawable().getConstantState();

		ConstantState image_30005_307_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_8_vide).getConstantState();
		ConstantState image_138_706_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_14_vide).getConstantState();
		ConstantState image_208_706_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_19_vide).getConstantState();
		ConstantState image_375_10_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_15_vide).getConstantState();
		ConstantState image_2075_13_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_20_vide).getConstantState();
		ConstantState image_3375_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_2_vide).getConstantState();
		ConstantState image_1375_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_7_vide).getConstantState();
		ConstantState image_386_17_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_13_vide).getConstantState();
		ConstantState image_20075_13_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_18_vide).getConstantState();
		ConstantState image_786_20_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_6_vide).getConstantState();
		ConstantState image_87_505_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_5_vide).getConstantState();
		ConstantState image_2086_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_11_vide).getConstantState();
		ConstantState image_27_086_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_16_vide).getConstantState();
		ConstantState image_0075_33_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_10_vide).getConstantState();
		ConstantState image_33_075_blanc=getResources().getDrawable(R.drawable.m5_mo12_ex1_chiffre_4_vide).getConstantState();

		if (! image_30005_307_Initiale.equals(image_30005_307_blanc) &&
				! image_138_706_Initial.equals(image_138_706_blanc) &&
				! image_208_706_Initial.equals(image_208_706_blanc) && 
				! image_375_10_Initial.equals(image_375_10_blanc) && 
				! image_2075_13_Initial.equals(image_2075_13_blanc) && 
				! image_3375_Initial.equals(image_3375_blanc) && 
				! image_1375_Initial.equals(image_1375_blanc) && 
				! image_386_17_Initial.equals(image_386_17_blanc) && 
				! image_20075_13_Initial.equals(image_20075_13_blanc) &&
				! image_786_20_Initial.equals(image_786_20_blanc) && 
				! image_87_505_Initial.equals(image_87_505_blanc) && 
				! image_2086_Initial.equals(image_2086_blanc) && 
				! image_27_086_Initial.equals(image_27_086_blanc) &&
				! image_0075_33_Initial.equals(image_0075_33_blanc) &&
				! image_33_075_Initial.equals(image_33_075_blanc))  
		{
			return true;
		}else
		{
			return false;	
		}
	}


	public boolean test_boolean()
	{
		if (boolean_33_075 && boolean_30005_307 && boolean_138_706 && boolean_208_706 
				&& boolean_375_10 && boolean_2075_13 && boolean_3375 && boolean_1375 &&
				boolean_386_17 && boolean_20075_13 && boolean_786_20 && boolean_87_505 &&
				boolean_2086 && boolean_27_086 && boolean_0075_33 )
		{
			return true;
		}else
		{
			return false;	
		}
	}


	public void try_stop_mp(MediaPlayer mp)
	{
		try {

			if (mp != null) {
				mp.stop();
				mp.release();
			}

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	protected void onRestart() {
		super.onRestart();
	}

	protected void onPause() {
		super.onPause();
	}

	public boolean isOnline() {

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo netInfo = cm.getActiveNetworkInfo();
		if (netInfo != null && netInfo.isConnectedOrConnecting()) {
			return true;
		}
		return false;
	}
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);
		Scalable.scaleContents(this);
	}


}